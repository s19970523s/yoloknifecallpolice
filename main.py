import threading
import time
from yolo import firstRun as yoloSet
from yolo import decectMydata
from transmissionFlaskServer import transmission
import os
import GUI
def main():
    transmissionThread = threading.Thread(target = transmission)#建立傳輸伺服端執行緒
    transmissionThread.start()#傳輸伺服端執行緒開始
    yoloSet()#yolo初始化yolo設定
    print("初始化完成")
    path='imgtest/ltuschool.jpg'
    while True:
        if os.path.isfile(path):
            anser=decectMydata(path)#開始辨識
            if(anser=="knife"):
               GUI.ui(path)
if __name__ == '__main__':
    main()